import os
import glob
import sys
import pathlib
import shutil


def paths_in_dir(directory):
    path_list = []
    for root, subdirs, files in os.walk(directory):
            for filename in files:
                file_path = os.path.join(root, filename)
                path_list.append(file_path)
    return path_list

def conv_enc_file(file_path, new_file_path):
    coding1 = "cp1251"
    coding2 = "utf-8"
    try:
        f = open(file_path, 'r', encoding=coding1)
        content = f.read()
        f.close()
        f = open(new_file_path, 'w+', encoding=coding2)
        f.write(content)
        f.close()
    except Exception as e:
        print("convert error", file_path)
        shutil.copyfile(file_path, new_file_path)


def conv_dir(dir_path):
    postfix = "_conv"
    head_tail = os.path.split(dir_path)

    dir_name = os.path.basename(dir_path)
    path_list = paths_in_dir(dir_path)
    for file_path in path_list:
        new_file_path = file_path.replace(
            dir_path, head_tail[0]+"/converted/"+head_tail[1])
        new_dir_name = os.path.dirname(new_file_path)
        if not os.path.exists(new_dir_name):
            pathlib.Path(new_dir_name).mkdir(parents=True, exist_ok=True)
        conv_enc_file(file_path, new_file_path)

mypath = "/home/tkn/Desktop/conv_enc/_buffertest"
conv_dir(mypath)
print("done")



